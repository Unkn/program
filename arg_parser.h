/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ARGPARSER_H
#define ARGPARSER_H

#include <unordered_map>
//#include <map>
#include <memory>
#include <string_view>
#include <functional>
#include <vector>

#include "arg_opt.h"
#include "Logger.h"

namespace unkn
{
    class arg_parser
    {
        public:
            enum : int
            {
                ID_HELP = -1;
            };

            arg_parser();
            arg_parser(const arg_parser &) = delete;
            arg_parser(arg_parser &&) = default;
            virtual ~arg_parser() { }

            arg_parser & operator=(const arg_parser &) = delete;
            arg_parser & operator=(arg_parser &&) = default;

            void usage() const;
            bool parse(int argc, char **argv);

            template< typename ArgOpt, typename... Args >
            void add_opt(int id, Args&&... args)
            {
                // Use a non-templatized subroutine to minimized the generated code
                add_opt_internal(
                        std::make_unique< basic_arg_opt >(
                            static_cast< basic_arg_opt * >(
                                new ArgOpt(std::forward<Args>(args)...))));
            }

            const basic_arg_opt & get_opt(int id) const;

        protected:
            virtual bool validate() { return true; }

        private:
            // TODO Toy with a trie since I've been asked about that data structure before
            // TODO rename to opt_lookup_ or somethin
            std::unordered_map< std::string_view, basic_arg_opt * > opt_map_;
            std::unordered_map< int, std::unique_ptr< basic_arg_opt > > opt_registry_;

            bool argIsVal(int argc, char **argv, int idx) const;
            void register_opt(const std::string & name, basic_arg_opt * opt);
            void add_opt_internal(int id, std::unique_ptr<basic_arg_opt> && opt);

    };
}

#endif
