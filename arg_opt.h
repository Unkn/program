/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ARGOPT_H
#define ARGOPT_H

#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
#include <limits>
#include <sstream>
#include <cstdint>

#include "Logger.h"

namespace unkn
{
    const int NO_ARGUMENT = 0;
    const int REQUIRED_ARGUMENT = 1;
    const int OPTIONAL_ARGUMENT = -1;

    class base_arg_opt
    {
        public:
            using arg_list = std::vector<const char *>;

            basic_arg_opt() = delete;
            basic_arg_opt(const arg_opt &) = delete;
            basic_arg_opt(arg_opt &&) = default;
            virtual ~basic_arg_opt() { }

            basic_arg_opt & operator=(const basic_arg_opt &) = delete;
            basic_arg_opt & operator=(basic_arg_opt &&) = default;

            const std::string &short_name() const { return short_name_; }
            const std::string &long_name() const { return long_name_; }
            const std::string &description() const { return desc_; }
            bool parsed() const { return parsed_; }
            int arg_count() const { return arg_count_; }

            bool parse(const arg_list & args = {});

            void print_help() const;

        protected:
            arg_opt(char short_name, int arg_count, std::string desc);
            arg_opt(std::string long_name, int arg_count, std::string desc);
            arg_opt(char short_name, std::string long_name, int arg_count, std::string desc);

            virtual bool parse_impl(const arg_list & args) = 0;

        private:
            std::string desc_;
            bool parsed_;
            int arg_count_;
            std::string short_name_;
            std::string long_name_;
    };

    using arg_opt_parser = std::function< bool(Value &, const basic_arg_opt::arg_list &) >;

    template< int ArgCount typename Value, Value Default = Value() >
    class arg_opt : public basic_arg_opt
    {
        public:
            arg_opt() = delete;
            arg_opt(const arg_opt &) = delete;
            arg_opt(arg_opt &&) = default;
            virtual ~arg_opt() { }

            arg_opt(char short_name, std::string desc, Value default_val) :
                base_arg_opt(short_name, ArgCount, desc),
                value_(default_val)
            { }

            arg_opt(std::string long_name, std::string desc, Value default_val) :
                base_arg_opt(long_name, ArgCount, desc),
                value_(default_val)
            { }

            arg_opt(char short_name, std::string long_name, std::string desc, Value default_val) :
                base_arg_opt(short_name, long_name, ArgCount, desc),
                value_(default_val)
            { }

            arg_opt & operator=(const arg_opt &) = delete;
            arg_opt & operator=(arg_opt &&) = default;

            const Value & value() const { return value_; }

        protected:
            Value value_ = Default;
    };

    class bool_arg_opt : public arg_opt< NO_ARGUMENT, bool, false >
    {
        public:
            virtual ~bool_arg_opt() { }

        protected:
            bool parse_impl(const arg_list & args) override
            {
                if (args.size() != 0)
                    return false;
                value_ = true;
                return true;
            }
    };

    class string_arg_opt : public arg_opt< REQUIRED_ARGUMENT, std::string >
    {
        public:
            virtual ~string_arg_opt() { }

        protected:
            bool parse_impl(const arg_list & args) override
            {
                if (args.size() != 1)
                    return false;
                value_ = args[0];
                return true;
            }
    };

    template< typename T, T Min = std::numeric_limits<T>::min(), T Max = std::numeric_limits<T>::max() >
    class primitive_arg_opt : public arg_opt< REQUIRED_ARGUMENT, T, 0 >
    {
        public:
            virtual ~primitive_arg_opt() { }

        protected:
            bool parse_impl(const arg_list & args) override
            {
                bool ret = false;
                T tmp;

                if (args.size() != 1)
                    LOG_ERROR("Invalid number of args");
                else if (!try_parse(*args.begin(), tmp))
                    LOG_ERROR("Failed to parse input");
                else if (tmp < Min || Max < tmp)
                    LOG_ERROR("Input out of range [{}, {}]: {}", Min, Max, tmp);
                else
                {
                    value_ = tmp;
                    ret = true;
                }

                return ret;
            }

            bool try_parse(const char * input, T & t) const
            {
                try
                {
                    std::stringstream ss(input);
                    // TODO _ACM_ Verify there is nothing left in the stream too
                    ss >> t;
                }
                catch (...)
                {
                    return false;
                }
                return true;
            }
    };

    using int8_arg_opt = primitive_arg_opt<int8_t>;
    using int16_arg_opt = primitive_arg_opt<int16_t>;
    using int32_arg_opt = primitive_arg_opt<int32_t>;
    using int64_arg_opt = primitive_arg_opt<int64_t>;
    using uint8_arg_opt = primitive_arg_opt<uint8_t>;
    using uint16_arg_opt = primitive_arg_opt<uint16_t>;
    using uint32_arg_opt = primitive_arg_opt<uint32_t>;
    using uint64_arg_opt = primitive_arg_opt<uint64_t>;
}

#endif
