/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "arg_opt.h"

namespace unkn
{
    bool base_arg_opt::parse(const arg_list & args)
    {
        bool ret = parse_impl(args);
        if (ret)
            parsed_ = true;
        return ret;
    }

    void base_arg_opt::print_help() const
    {
        int len = short_name_.size() + long_name_.size() + 2;

        std::cerr << "  ";

        if (!short_name_.empty())
            std::cerr << short_name_;

        if (!short_name_.empty() && !long_name_.empty())
        {
            std::cerr << ", ";
            len += 2;
        }

        if (!long_name_.empty())
            std::cerr << long_name_;

        // TODO Look into iomanip stuff that can be done instead
        const std::string fill20(20, ' ');

        // Require at least 4 spaces between the opts and the
        // description with descriptions left aligned at column 20
        if (len > 16)
            std::cerr << "\n" << fill20;
        else
            std::cerr << std::string(20 - len, ' ');

        for (std::string::size_type i = 0; i < desc_.size(); i += 60)
        {
            const int len = std::min(60, desc.size() - i);
            std::string_view chunk(desc_.data() + i, len);

            std::cerr << fill20 << chunk << "\n";
        }
    }

    base_arg_opt::base_arg_opt(char short_name, int arg_count, std::string desc) :
        base_arg_opt(short_name, "", arg_count, desc)
    { }

    base_arg_opt::arg_opt(std::string long_name, int arg_count, std::string desc) :
        base_arg_opt('\0', long_name, arg_count, desc)
    { }

    base_arg_opt::arg_opt(char short_name, std::string long_name, int arg_count, std::string desc) :
        desc_(desc),
        parsed_(false),
        arg_count_(arg_count),
        short_name_(),
        long_name_(),
    {
        if (arg_count < -1)
            throw std::invalid_argument("Invalid argument count");

        if (short_name == '\0' && long_name.empty())
            throw std::invalid_argument("At least one of a short name or a long name is required");

        if (desc.empty())
            throw std::invalid_argument("An argument description is required");

        if (short_name != '\0')
        {
            if (!isalnum(short_name))
                throw std::invalid_argument("Short opt is required to be alphanumeric");

            short_name_ = "-";
            short_name_.append(1, short_name);
        }

        if (!long_name.empty())
        {
            if (long_name.length() < 2)
                throw std::invalid_argument("Long opt is required to be at least 2 characters");
            else if (long_name.front() == '_' || long_name.back() == '_' || long_name.front() == '-' || long_name.back() == '-')
                throw std::invalid_argument("Long opt cannot start or end with an underscore");
            else if (!std::all_of(long_name.begin(), long_name.end(), [](const char &c){ return c == '_' || c == '-' || isalnum(c); }))
                throw std::invalid_argument("Long opt can only consist of alphanumeric and underscore characters");

            long_name_ = "--";
            long_name_ += long_name;
        }
    }
}
