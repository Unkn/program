/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "arg_parser.h"

namespace unkn
{
    arg_parser::arg_parser()
    {
        add_opt<bool_arg_opt>(ID_HELP, 'h', "help", "Display this help info")
    }

    void arg_parser::usage() const
    {
        // Iterating through a hash table is slow, but 'usage' is the only thing
        // that does it. Furthermore 'usage' is only expected to be retrieved
        // immediately before a program exit so we don't really care with respect
        // to other uses of the structure.
        for (const auto & opt : opt_registry_)
            opt.second->print_help();
    }

    bool arg_parser::parse(int argc, char **argv)
    {
        bool ok = true;

        for (int i = 1; ok && i < argc; ++i)
        {
            // NOTE Don't use getopt_long unless we porting it

            // TODO This is gonna create a string from the const char so its a
            // wasted alloc but who cares right now...
            auto it = opt_map_.find(std::string_view(argv[i]));
            if (it == opt_map_.end())
            {
                LOG_DEBUG("Processed invalid argument '{}'", argv[i]);
                ok = false;
                break;
            }

            auto opt = it->second;
            int expect = opt->arg_count();
            std::vector<const char *> args;

            if (expect == NO_ARGUMENT)
            {
                // Just passing through...
            }
            else if (expect == OPTIONAL_ARGUMENT)
            {
                if (argIsVal(argc, argv, i + 1))
                {
                    args.emplace_back(argv[i + 1]);
                    ++i;
                }
            }
            else if (expect == REQUIRED_ARGUMENT)
            {
                if (argIsVal(argc, argv, i + 1))
                {
                    args.emplace_back(argv[i + 1]);
                    ++i;
                }
                else
                {
                    LOG_DEBUG("Required argument missing for option {}", argv[i]);
                    ok = false;
                }
            }
            else
            {
                assert(!">1 arg not implemented");
                ok = false;
            }

            if (ok)
                ok = opt->parse(args);
        }

        if (ok)
        {
            auto & help = get_opt(ID_HELP);
            if (help.parsed())
            {
                LOG_DEBUG("Help option was parsed");
                ok = false;
            }
            else if (!validate())
            {
                LOG_DEBUG("Failed to validate command line arguments");
                ok = false;
            }
        }

        if (!ok)
            usage(/*argv[0]*/);

        return ok;
    }

    const basic_arg_opt & arg_parser::get_opt(int id) const
    {
        auto it = opt_registry_.find(id);
        // Whatever is using an arg_parser should have complete knowledge of the
        // ID's available
        assert(it != opt_registry_.end());
        return *it->second();
    }

    bool arg_parser::argIsVal(int argc, char **argv, int idx) const
    {
        // TODO This isn't right :|
        return idx < argc && *argv[idx] != '-';
    }

    void arg_parser::register_opt(const std::string & name, basic_arg_opt * opt)
    {
        assert(opt != nullptr);

        if (!name.empty())
            opt_map_.emplace(name, opt);
    }

    void arg_parser::add_opt_internal(int id, std::unique_ptr<basic_arg_opt> && opt)
    {
        // TODO Check that this will get optimized out in release builds. If not,
        // then call them explicitly in each assertion.
        const std::string &sname = opt->getShortName();
        const std::string &lname = opt->getLongName();

        // Whatever is registering args should be aware that collistions are not to
        // exist.
        assert(sname.empty() || opt_map_.find(sname) == opt_map_.end());
        assert(lname.empty() || opt_map_.find(lname) == opt_map_.end());
        // This should have been caught at the time of argument construction, but
        // no harm in asserting here that it doesn't make sense to register
        // something that isn't able to be set.
        assert(!sname.empty() || !lname.empty());
        // Make sure that the registered opt id is unique. If there is a collision,
        // the developer made a mistake.
        assert(opt_registry_.find(id) == opt_registry_.end());

        opt_registry_.emplace(id, std::move(opt));

        auto & ropt = get_opt(id);
        register_opt(ropt.short_name(), ropt);
        register_opt(ropt.long_name(), ropt);
    }
}
