/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PROGRAM_H
#define PROGRAM_H

namespace unkn
{
    struct null_arg_parser
    {
        bool parse(int, char **) { return true; }
        void usage() { /* pinapples */ }
    };

    template< typename ArgParser = null_arg_parser >
    class program
    {
        public:
            program() = default;
            virtual ~program() { }

            int main(int argc, char **argv);
            {
                if (!arg_parser_.parse(argc, argv))
                {
                    arg_parser_.usage();
                    return -1;
                }

                return run();
            }

        protected:
            virtual int run() = 0;

            const ArgParser & arg_parser() const { return arg_parser_; }

        private:
            ArgParser arg_parser_;
    };
}

#endif
